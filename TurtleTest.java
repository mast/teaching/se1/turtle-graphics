

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;
import java.awt.*;

/**
 * The test class TurtleTest.
 *
 * @author  Volodymyr Biryuk
 * @version 30. November 2023
 */
public class TurtleTest
{
    private Turtle _turtle;
    
    /**
     * Default constructor for test class TurtleTest
     */
    public TurtleTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @BeforeEach
    public void setUp()
    {
        _turtle = new Turtle();
    }
    
    @Test
    public void farbenTest()
    {
        Color[] expected = 
        {
            Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY, Color.GRAY, Color.GREEN,
            Color.LIGHT_GRAY, Color.MAGENTA, Color.PINK, Color.YELLOW
        };
        
        Color[] actual = _turtle.gibFarben();
        assertEquals(expected.length, actual.length);
        for(int i = 0; i < actual.length; i++)
        {
            Color actual_ = actual[i];
            Color expected_ = expected[i];
            assertEquals(expected_, actual_);
        }
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @AfterEach
    public void tearDown()
    {
    }
}
